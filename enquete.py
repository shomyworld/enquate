from getpass import getpass
import requests
from bs4 import BeautifulSoup
import re
import random
import time


def getSoup(r):
    html = r.content
    soup = BeautifulSoup(html, "html.parser")
    return soup


start = time.time()

s = requests.session()

Id = input("input username > ")
password = getpass("input password > ")

# 認証情報
login_info = {
    "id": Id,
    "pass": password
    }

word = ["" for _ in range(6)]   # textareaのワード
url = "http://www2.jn.iwasaki.ac.jp:8080/enqueteH23/{}"
s.post(url.format('auth.php'),  data=login_info)  # ログイン処理
r = s.get(url.format('enqueteSelect.php'))
soup = getSoup(r)
print('login {} '.format(['success', 'failed'][0 if soup.find("a") else 1]))

enq_total = len(soup.find_all("input"))   # アンケートの総数
link = []
for i in range(enq_total):
    if soup.find_all("input")[i].get("value") == "回答する":
        link.append(soup.find_all("input")[i].get("onclick"))

# 未回答アンケートの『locaiton.href』部分を抽出
query = []
c = 0
for i in range(len(link)):
    query.append(re.split('"', link[i])[1])
    for i in range(len(query)):
        URL = "http://www2.jn.iwasaki.ac.jp:8080/enqueteH23/"
        hidden = []
        radio = []
        URL += query[i]
        r = s.get(URL)   # アンケートページ取得
        soup2 = getSoup(r)
        hidden = soup2.find_all("input", type="hidden")  # hidden属性がある要素を取得
        input = soup2.find_all("input")
        textarea = soup2.find_all("textarea")
        se1 = set()
        se2 = set()
        for i in input:
            se1.add(i.get("name"))   # input要素のname属性の値を取得し、セットに入れて重複除外
        for h in textarea:
            se2.add(h.get("name"))  # textarea要素のname属性の値を取得し、セットに入れて重複除外
        key1 = list(se1)
        key2 = list(se2)
        value1 = []
        value2 = []
        for j in range(len(key1)):
            value1.append(random.randint(1, 5))         # ラジオボタンの値をランダムに設定
        for l in range(len(key2)):
            value2.append(word[random.randint(0, len(word)-1)])  # textareaをランダムなワードに設定
        dic1 = dict(zip(key1, value1))     # keyとvalueで辞書作成
        dic2 = dict(zip(key2, value2))
        dic1.update(dic2)
        for k in range(len(hidden)):
            dic1[hidden[k].get("name")] = hidden[k].get("value")
        if None in dic1:
            del dic1[None]
        r4 = s.post("http://www2.jn.iwasaki.ac.jp:8080/enqueteH23/enqueteRegist.php", data=dic1)  # アンケートに回答
    c += 1
    print("進行状況", c, "/", len(link))
print("すべてのアンケートに回答しました")

process_time = time.time() - start
print(('実行時間は{}').format(process_time))
